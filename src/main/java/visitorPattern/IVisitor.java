package visitorPattern;

/**
 * Created by PeiXingzhou on 2015/8/23.
 */
public interface IVisitor {


    void visit(ConcreteElement1 concreteElement1);

    void visit(ConcreteElement2 concreteElement2);

}
