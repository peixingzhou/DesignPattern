package visitorPattern;

/**
 * Created by PeiXingzhou on 2015/8/23.
 */
public interface IElement {
    public void accept(IVisitor visitor);
}
