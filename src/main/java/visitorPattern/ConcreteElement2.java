package visitorPattern;

/**
 * Created by us PeiXingzhou 2015/8/23.
 */
public class ConcreteElement2 implements IElement {
    @Override
    public void accept(IVisitor visitor) {
        visitor.visit(this);
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }

}
