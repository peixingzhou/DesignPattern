package visitorPattern;

/**
 * Created by PeiXingzhou on 2015/8/23.
 */
public class Client {
    public static void main(String[] args) {
        IElement element1=new ConcreteElement1();
        IElement element2=new ConcreteElement2();

        ObjectStructure objectStructure=new ObjectStructure();
        objectStructure.attach(element1);
        objectStructure.attach(element2);

        objectStructure.operateBy(new ConcreteVisitor1());

    }
}
