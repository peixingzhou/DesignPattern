package visitorPattern;

/**
 * Created by us PeiXingzhou 2015/8/23.
 */
public class ConcreteVisitor2 implements IVisitor {
    @Override
    public void visit(ConcreteElement1 element) {
        System.out.println(this.toString() + "-->" + element.toString());
    }

    @Override
    public void visit(ConcreteElement2 element) {
        System.out.println(this.toString() + "-->" + element.toString());
    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }

}
