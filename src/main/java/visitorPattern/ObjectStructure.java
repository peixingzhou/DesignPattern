package visitorPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by us PeiXingzhou 2015/8/23.
 */
public class ObjectStructure {
    private List<IElement> elementList = new ArrayList<>();

    public void attach(IElement element) {
        elementList.add(element);
    }

    public void remove(IElement element) {
        elementList.add(element);
    }

    public void operateBy(IVisitor visitor) {
        for (IElement element : elementList) {
            element.accept(visitor);
        }
    }
}
