package observerPattern;

/**
 * Created by PeiXingzhou on 2015/8/24.
 */
public class ObserverImpl02 implements IObserver {
    @Override
    public void update() {
        System.out.println(this.getClass().getName());
    }
}
