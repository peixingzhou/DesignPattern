package observerPattern;

/**
 * Created by PeiXingzhou on 2015/8/24.
 */
public interface ISubject {
    public void addObserver(IObserver observer);
    public void removeObserver(IObserver observer);
    public void operateObserver();
}
