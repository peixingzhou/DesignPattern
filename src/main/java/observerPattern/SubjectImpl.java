package observerPattern;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by PeiXingzhou on 2015/8/24.
 */
public class SubjectImpl implements ISubject {
    private List<IObserver> list = null;

    public SubjectImpl() {
        list=new ArrayList<>();
    }

    @Override
    public void addObserver(IObserver observer) {
        list.add(observer);
    }

    @Override
    public void removeObserver(IObserver observer) {
        list.remove(observer);
    }

    @Override
    public void operateObserver() {
        for (IObserver observer : list) {
            observer.update();
        }
    }
}
