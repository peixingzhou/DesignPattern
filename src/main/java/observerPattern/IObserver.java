package observerPattern;

/**
 * Created by PeiXingzhou on 2015/8/24.
 */
public interface IObserver {
    public void update();
}
