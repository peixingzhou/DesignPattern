package observerPattern;

/**
 * Created by PeiXingzhou on 2015/8/24.
 */
public class Client {

    public static void main(String[] args) {
        IObserver observer01 = new ObserverImpl01();
        IObserver observer02 = new ObserverImpl02();

        ISubject subject = new SubjectImpl();

        subject.addObserver(observer01);
        subject.addObserver(observer02);

        subject.operateObserver();
        System.out.println("------------------");
        subject.removeObserver(observer02);
        subject.operateObserver();


    }
}
